from matplotlib import animation
import matplotlib.pyplot as plt
import numpy.random as rd
import numpy as np

from lignes import adjust_spines
from cercle import drop_draw, drop_coor

def rain_unpack(rain):
    """Turn a packed file into a rain to put into a film"""
    rain_u = []
    time_max = 0
    # Determine the maximum time
    for items in rain.items():
        time_max = max(time_max, items[0][-1] + items[1][-1])

    for items in rain.items():
        # Unpack the item
        xpos, ypos, time = items[0]
        big_init, duration = items[1]
        it = range(time)
        remain_it = range(time_max - (time + duration))
        shape = drop_coor(xpos, ypos, big_init)
        drop_t = [(shape, duration) for i in range(duration)]
        rain_line = [None for i in it] + drop_t + [None for i in remain_it]
        rain_u.append(rain_line)

    return rain_u


def film():
    """Make a film of a rain"""
    global ax, rain_u, cstep
    fig, ax = plt.subplots(figsize=(10,10))
    # Rain format {(xpo, ypos, time): (big_init, duration)}
    # rain = {(0, 0, 50):(1, 100), (0, 0, 200):(2, 150)}  # Packed format of a rain
    rain = {(rd.uniform(-9, 9), rd.uniform(-9, 9), rd.randint(500)): (rd.uniform(0.1, 1), rd.randint(10, 200)) for i in range(900)}  # Packed format of a rain
    #rain = {(rd.uniform(-9, 9), rd.uniform(-9, 9), rd.randint(900)): rd.randint(100) for i in range(900)}  # Packed format of a rain
    rain_u = rain_unpack(rain)
    nframes = len(rain_u[0])
    cstp = [0 for i in range(len(rain_u))]
    def animate(i):
        global ax, rain_u, cstep
        ax.clear()
        ax.set_xlim(-10, 10)
        ax.set_ylim(-10, 10)
        for j, rain_line in enumerate(rain_u):
            if rain_line[i]:
                ax = drop_draw(ax, rain_line[i][0], cstp[j], rain_line[i][1]+1) #The +1 seems to have solved a bug
                cstp[j] += 1


        adjust_spines(ax, [''])

    anim = animation.FuncAnimation(fig, animate, frames=nframes)
    anim.save('drop.mp4', fps=24, extra_args=['-vcodec', 'libx264'])
    plt.close()

film()
