# Lignes
The goal of this project is to draw textured lines.
Computer mostly draw perfectly straight lines like these ones.

![Computer drawn lines](https://framagit.org/APianoInterpreter/lignes/-/raw/master/lines_0.png)

Human draw lines with texture even if they use a ruler.

![Computer drawn lines](https://framagit.org/APianoInterpreter/lignes/-/raw/master/lines_0.002.png)

# Requirements
Numpy and Matplotlib these are *essential libraries* 

# Generating lines
simply launch lignes.py with ```python lignes.py```
