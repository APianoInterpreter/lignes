import matplotlib.pyplot as plt
import numpy.random as rd
import numpy as np
from math import pi, cos, sin
from scipy.signal import savgol_filter

from lignes import adjust_spines

def rand_ev_c(seed, low, high, nbpt, scale=0.01):
    """Generate a CIRCULAR random sequence using gaussian noise

    Parameters
    ----------
    low: float, minimal value of the sequence
    high: float, maximal value
    nstp: int, number of elements in the sequence
    seed: float, initial value of the sequence
    loc: float, the mean value of the gaussian noise
    scale: float, the standard deviation

    Output
    ------
    rands: ints list, the entire sequence between low and high of size nstp

    """
    rand = [seed]
    # Part where we extend the sequence using gaussian noise
    while len(rand) < nbpt:
        seed += rd.normal(0, scale)  # Generate the next value

        if seed < low:
            seed = low
            rand += [seed]
        elif seed > high:
            seed = high
            rand += [seed]
        else:
            rand += [seed]

        # Part where we shrink the sequence from the beginning
        c0 = np.abs(rand[0] - rand[-1]) > scale/5
        if len(rand) == nbpt and c0:
            rand = rand[1:]

    return rand


def gen_coordinates(big, pos, crl=0.04, cosinus=True, smooth=True):
    """Generate one dimension of the coordinates for the circle
    Parameters
    ----------
    big: float, size of the circle
    pos: floats, coordinate of the circle
    crl: float, control how round the circle is
    smooth: Boolean, smooth or not the circle
    """
    nbpt = 100  # A 100 points suffices to make a nice circle
    # The max is to avoid negative diameters
    diam = rand_ev_c(0.5, max(0.5-crl, crl), 0.5+crl, nbpt=nbpt)
    if cosinus:
        coor = pos + big*(diam*np.array([cos(i) for i in np.linspace(0, 2*pi, nbpt)]))
    else:
        coor = pos + big*(diam*np.array([sin(i) for i in np.linspace(0, 2*pi, nbpt)]))
    coor = np.concatenate((coor, coor[:50])) # Concatenate with the previous coordinates to make it more roundish
    if smooth:
        # Smooth the lines to have nice circles
        coor = savgol_filter(coor, 11, 3) # ndow size 11, polynomial order 3
    return coor



def cercle_coor(big=1, xpos=0, ypos=0):
    """Generate the coordinates of the circle

    Parameters
    ----------
    big: float, size of the circle
    (xpos, ypos): floats, coordinates of the circle
    crl: float, control how round the circle is
    smooth: Boolean, smooth or not the circle

    Output
    ------
    shape: couple of float which are the coordinates of the circle
    """
    x = gen_coordinates(big, xpos)
    y = gen_coordinates(big, ypos, cosinus=False)
    return (x, y)


def drop_coor(xpos=0, ypos=0, big_init=0.5):
    """Generate two sets of coordinates for the end and beginning"""
    big_end = big_init*3
    x_init, y_init = cercle_coor(big_init, xpos, ypos)
    x_end, y_end = cercle_coor(big_end, xpos, ypos)
    return (x_init, y_init, x_end, y_end)


def drop_draw(ax, shape, cstp, nstp):
    """Draw the drops depending on its coordinates and the step (when it fell)"""
    if shape:
        xi, yi, xe, ye = shape
    else:
        return ax
    alphas = np.linspace(1, 0, nstp) # Make an array of size nsteps
    x_int = (cstp*xe + (nstp-cstp)*xi)/nstp
    y_int = (cstp*ye + (nstp-cstp)*yi)/nstp
    ax.fill(x_int, y_int, alpha=alphas[cstp], lw=0, color="black")
    adjust_spines(ax, [])
    return ax


def drop(ax=None, xpos=0, ypos=0, nstp=10):
    """Draw an inkdrop"""
    if ax == None:
        fig, ax = plt.subplots(figsize=(10, 10))
    shape = drop_coor(xpos, ypos)
    for i in range(nstp):
        ax = drop_draw(ax, shape, i, nstp)
    plt.show()


def cercle_draw(shape, ax, goutte=None):
    """Draw the circle using its coordinates"""
    x, y = shape
    if goutte:
        ax.fill(x, y, alpha=goutte, color="black", edgecolor=None)
    else:
        ax.plot(x, y, alpha=1, color="black", lw=2)
    adjust_spines(ax, [])
    return ax, x, y


def un_cercle(pos=(0,0), goutte=None):
    """ Generate its coordinate and Draw a Circle. A wrap function
    """
    xpos, ypos = pos
    if goutte:
        big = rd.random()
    else:
        big=1
    fig, ax = plt.subplots(figsize=(5,5))
    ax, x, y = cercle_draw(cercle_coor(big, xpos, ypos), ax, goutte)
    plt.savefig("cercle.png", dpi=300)
    plt.close()


def pluie(ngouttes=10, xfsiz=30, yfsiz=10):
    """ Draw a rain with ngouttes drops save it and close it"""
    fig, ax = plt.subplots(figsize=(xfsiz, yfsiz))
    ax.set_ylim(-0.55, yfsiz+0.55)
    ax.set_xlim(-0.55, xfsiz+0.55)
    xpos_max = 0
    for i in range(ngouttes):
        #xpos = rd.uniform(1, xfsiz-1)
        #xpos = rd.normal((xfsiz-1)/2, scale=5)
        xpos = rd.exponential()*(xfsiz-20)
        ypos = rd.uniform(1, yfsiz-1)
        big = rd.uniform(0.1, 1)
        alpha = rd.uniform(0.3, 1)
        ax, x, y = cercle_draw(cercle_coor(big, xpos, ypos), ax, goutte=alpha)
    plt.savefig("pluie.png", dpi=300)
    plt.close()


circularity = False
circle = False
rain = False
drop = True

if __name__ == "__main__":
    if circularity:
        for i in range(10):
            # See that we generate circular sequences
            rand = rand_ev_c(5, 0, 10, 100)
            print(rand[0] - rand[-1])
    if circle:
        un_cercle()
    if rain:
        pluie(1000)
    if drop:
        coor = drop_coor()
        fig, ax = plt.subplots(figsize=(10,10))
        ax = drop_draw(ax, coor, 1, 10)
        ax = drop_draw(ax, coor, 5, 10)
        ax.set_xlim(-2, 2)
        ax.set_ylim(-2, 2)
        plt.show()



