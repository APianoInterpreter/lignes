import matplotlib.pyplot as plt
import numpy.random as rd
import numpy as np
from scipy.signal import savgol_filter


def rand_ev(low, high, nstp, seed, loc=0, scale=0.1):
    """Generate a sequence following a random evolution using gaussian noise

    Parameters
    ----------
    low: float, minimal value of the sequence
    high: float, maximal value
    nstp: int, number of elements in the sequence
    seed: float, initial value of the sequence
    loc: float, the mean value of the gaussian noise
    scale: float, the standard deviation

    Output
    ------
    rands: ints list, the entire sequence between low and high of size nstp

    """
    rands = []
    for i in range(nstp):
        seed += rd.normal(loc, scale)  # Generate the next value
        # The two bounds are sticky seed cannot go beyond them.
        if seed < low:
            rands += [low]
            seed = low
        elif seed > high:
            rands += [high]
            seed = high
        else:
            rands += [seed]  # Add the int to the list

    return rands


def adjust_spines(ax, spines):
    """removing the spines from a matplotlib graphics.
    taken from matplotlib gallery anonymous author.

    parameters
    ----------
    ax: a matplolib axes object
        handler of the object to work with
    spines: list of char
        location of the spines

    """
    for loc, spine in ax.spines.items():
        if loc in spines:
            pass
            # print 'skipped'
            # spine.set_position(('outward',10)) # outward by 10 points
            # spine.set_smart_bounds(true)
        else:
            spine.set_color('none')
            # don't draw spine
            # turn off ticks where there is no spine

    if 'left' in spines:
        ax.yaxis.set_ticks_position('left')
    else:
        # no yaxis ticks
        ax.yaxis.set_ticks([])

    if 'bottom' in spines:
        ax.xaxis.set_ticks_position('bottom')
    else:
        # no xaxis ticks
        ax.xaxis.set_ticks([])


def lines(var, smooth=True, nlines=10):
    """Generate a set of textured parallel lines"""
    nbpt = 1000  # Number of dots defining a line
    fig, ax = plt.subplots(figsize=(7, 5))
    mg = 0.5  # Space betwwen lines
    lw = 2  # Thicknes of a line
    for seed in range(nlines):
        line = rand_ev(seed-mg, seed+mg, nbpt, seed, scale=var)
        if smooth:
            line = savgol_filter(line, 51, 3) # window size 51, polynomial order 3
        ax.plot(range(nbpt), np.array(line), lw=lw, color="black")
    adjust_spines(ax, [''])  # Remove the axis
    plt.ylim(-mg, nlines + mg)
    plt.xlim(0, nbpt)
    plt.savefig("lines_%s.png" %(var), dpi=300)


if __name__ == "__main__":
    lines(0.005)
